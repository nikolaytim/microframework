<?php

namespace App\Factory;

use App\Exception\ApplicationException;

class Factory
{
    public static function create($class, ...$args)
    {
        if (!class_exists($class)) {
            throw new ApplicationException('Класс: ' . $class . ' не найден!');
        }

        return new $class(...$args);
    }
}