<?php

namespace App;

use App\Exception\ApplicationException;
use App\Factory\Factory;
use App\CommandProcessor\CommandProcessorInterface;
use App\Routing\RouteResolverInterface;
use App\Storage\StorageInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

class Application
{
//    private Request $request;
    private array $configuration = [];
    private StorageInterface $storage;
    private ContainerInterface $container;

    public function __construct()
    {
//        $this->request = Request::createFromGlobals();
        $this->configuration = $this->getConfiguration();
        $this->storage = Factory::create(
            $this->configuration['microframework']['require']['App\Storage\StorageInterface']
        );
        $this->container = Factory::create(
            $this->configuration['microframework']['require']['Psr\Container\ContainerInterface']
        );

        $this->bootContainer();
    }

    private function getConfiguration(): array
    {
        $configuration['services'] = Yaml::parseFile(dirname(__DIR__) . '/config/services.yaml');
        $configuration['routes'] = Yaml::parseFile(dirname(__DIR__) . '/config/routes.yaml');
        $configuration['microframework'] = Yaml::parseFile(dirname(__DIR__) . '/config/microframework.yaml');

        return $configuration;
    }

    private function bootContainer(): void
    {
        $commandProcessorClass = $this->configuration['microframework']['require']['App\CommandProcessor\CommandProcessorInterface'];
        $this->container->set('App\CommandProcessor\CommandProcessorInterface', function () use ($commandProcessorClass) {
            return $commandProcessorClass;
        });
    }

    public function handle(Request $request): void
    {
//        dump($request);
        try {
            $routeResolver = Factory::create(
                $this->configuration['microframework']['require']['App\Routing\RouteResolverInterface'],
                $request,
                $this->configuration
            );
            $routeConfiguration = $routeResolver->resolve();

            $commandProcessor = Factory::create(
                $this->configuration['microframework']['require']['App\CommandProcessor\CommandProcessorInterface'],
                $this->storage, 
                $this->container,
                $routeConfiguration,
                $request
            );
            $commandProcessor->execute();
        } catch (\Exception|\Throwable $exception) {
            echo $exception->getMessage();
            /**
             * Добавить обработку исключений
             */
            //(new ExceptionHandler($queue))->handler($command, $exception);
        }
    }
}