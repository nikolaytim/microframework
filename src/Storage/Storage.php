<?php

namespace App\Storage;

class Storage implements StorageInterface
{
    public function getProperty(string $nameProperty)
    {
        return $this->$nameProperty;
    }

    public function setProperty(string $nameProperty, $value): void
    {
        $this->$nameProperty = $value;
    }
}