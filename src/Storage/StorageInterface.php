<?php

namespace App\Storage;

interface StorageInterface
{
    public function setProperty(string $nameProperty, $value): void;
    public function getProperty(string $nameProperty);
}