<?php

namespace App\Command;

use App\Storage\StorageInterface;
use Symfony\Component\HttpFoundation\Request;

class SetQueryToStorageCommand implements CommandInterface
{
    private StorageInterface $storage;
    private Request $request;

    public function __construct(StorageInterface $storage, Request $request)
    {
        $this->storage = $storage;
        $this->request = $request;
    }

    public function execute(): void
    {
        foreach ($this->request->query->all() as $item => $value) {
            $this->storage->setProperty($item, $value);
        }
    }
}