<?php

namespace App\Command;

use App\Storage\StorageInterface;
use Symfony\Component\HttpFoundation\Request;

class TestCommand2 implements CommandInterface
{
    private StorageInterface $storage;
    private Request $request;

    public function __construct(StorageInterface $storage, Request $request)
    {
        $this->storage = $storage;
        $this->request = $request;
    }

    public function execute(): void
    {
//        echo '<p>TestCommand2</p>';
        $this->storage->setProperty('TestCommand2', 'Тестовые данные из TestCommand2');
    }
}