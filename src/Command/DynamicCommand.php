<?php

namespace App\Command;

use App\Factory\Factory;
use App\Storage\StorageInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class DynamicCommand implements CommandInterface
{
    private StorageInterface $storage;
    private ContainerInterface $container;
    private string $propertyStorage;
    private array $configuration;
    private Request $request;

    public function __construct(StorageInterface $storage, ContainerInterface $container, string $propertyStorage, array $configuration, Request $request)
    {
        $this->storage         = $storage;
        $this->container       = $container;
        $this->propertyStorage = $propertyStorage;
        $this->configuration   = $configuration;
        $this->request         = $request;
    }

    public function execute(): void
    {
        $valuePropertyStorage = $this->storage->getProperty($this->propertyStorage);
        if (!$valuePropertyStorage) {
            return;
        }

        foreach ($this->configuration as $value => $options) {
            if ($value == $valuePropertyStorage) {
                $commandProcessorClass = $this->container->get('App\CommandProcessor\CommandProcessorInterface')();
                $commandProcessor = Factory::create(
                    $commandProcessorClass,
                    $this->storage, 
                    $this->container,
                    $options,
                    $this->request
                );
                $commandProcessor->execute();
            }
        }
    }
}