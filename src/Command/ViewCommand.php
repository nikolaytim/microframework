<?php

namespace App\Command;

use App\Storage\StorageInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ViewCommand implements CommandInterface
{
    private StorageInterface $storage;
    private Request $request;

    public function __construct(StorageInterface $storage, Request $request)
    {
        $this->storage = $storage;
        $this->request = $request;
    }

    public function execute(): void
    {
        ob_start();
        echo '<pre>';
        var_dump($this->storage);
        echo '</pre>';
        $html = ob_get_clean();

        $response = new Response($html);
        $response->send();
    }
}