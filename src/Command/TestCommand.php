<?php

namespace App\Command;

use App\Storage\StorageInterface;
use Symfony\Component\HttpFoundation\Request;

class TestCommand implements CommandInterface
{
    private StorageInterface $storage;
    private Request $request;

    public function __construct(StorageInterface $storage, Request $request)
    {
        $this->storage = $storage;
        $this->request = $request;
    }

    public function execute(): void
    {
//        echo '<p>TestCommand</p>';
        $this->storage->setProperty('TestCommand', 'Тестовые данные из TestCommand');
//        $this->storage->setProperty('state', 1);
    }
}