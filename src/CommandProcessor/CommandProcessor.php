<?php

namespace App\CommandProcessor;

use App\Factory\Factory;
use App\Command\MacroCommand;
use App\Command\DynamicCommand;
use App\Exception\CommandException;
use App\Exception\CommandProcessorException;
use App\Storage\StorageInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class CommandProcessor implements CommandProcessorInterface
{
    private StorageInterface $storage;
    private ContainerInterface $container;
    private array $routeConfiguration;
    private Request $request;

    public function __construct(StorageInterface $storage, ContainerInterface $container, array $routeConfiguration, Request $request)
    {
        $this->storage            = $storage;
        $this->container          = $container;
        $this->routeConfiguration = $routeConfiguration;
        $this->request            = $request;
    }
    
    private function createCommands(): array
    {
        $commands = [];

        foreach ($this->routeConfiguration as $item) {
            if (is_array($item)) {
                foreach ($item as $propertyStorage => $conditions) {
                    $command = new DynamicCommand(
                        $this->storage, $this->container, $propertyStorage, $conditions, $this->request
                    );
                }
            } else {
                $command = new $item($this->storage, $this->request);
            }

            $commands[] = $command;
        }

        return $commands;
    }

    public function execute(): void
    {
        $commands = $this->createCommands();

//        dump('commands', $commands);

        $macroCommand = Factory::create(MacroCommand::class, $commands);

//        dump('macroCommand', $macroCommand);

        $macroCommand->execute();
    }
    
}