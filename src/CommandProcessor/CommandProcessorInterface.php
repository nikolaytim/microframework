<?php

namespace App\CommandProcessor;

interface CommandProcessorInterface
{
    public function execute(): void;
}