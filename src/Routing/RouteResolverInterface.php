<?php

namespace App\Routing;

interface RouteResolverInterface
{
    public function resolve(): array;
}