<?php

namespace App\Routing;

use Symfony\Component\HttpFoundation\Request;

class RouteResolver implements RouteResolverInterface
{
    private Request $request;
    private array $configuration;

    public function __construct(Request $request, array $configuration = [])
    {
        $this->request = $request;
        $this->configuration = $configuration;
    }

    public function resolve(): array
    {
        $method = $this->request->getMethod();
        $route = $this->request->getPathInfo();
        if (strlen($route) > 1) {
            $route = rtrim($route, '/');
        }

        if (array_key_exists($route, $this->configuration['routes']) &&
            array_key_exists($method, $this->configuration['routes'][$route])) {
            return $this->configuration['routes'][$route][$method];
        }

        if (array_key_exists(404, $this->configuration['routes'])) {
            return $this->configuration['routes'][404]['GET'];
        }

        return [];
    }
}