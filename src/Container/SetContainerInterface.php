<?php

namespace App\Container;

use Psr\Container\ContainerInterface;

interface SetContainerInterface
{
    public function setContainer(ContainerInterface $container): void;
}