<?php

namespace App\Container;

use App\Exception\ContainerException;
use Psr\Container\ContainerInterface;

class Container implements ContainerInterface
{
    private array $container = [];

    public function get(string $id): callable
    {
        if (!array_key_exists($id, $this->container)) {
            throw new ContainerException('Свойство: ' . $id . ' не найдено в Container!');
        }

        return $this->container[$id];
    }

    public function has(string $id): bool
    {
        return array_key_exists($id, $this->container);
    }

    public function set(string $id, callable $value)
    {
        $this->container[$id] = $value;
    }

/*
    public function resolve(string $key, ...$args)
    {
        if ($this->has($key)) {
            $callback = $this->get($key);
        }

        /**
         * callback уже зарегистрирован - разрешаем зависимость
         *
        if ($callback && is_callable($callback)) {
            try {
                return $callback(...$args);
            } catch (\TypeError|\Exception $exception) {
                throw new ContainerException(
                    'Ошибка при разрешении зависимости - ' . $key . ': ' . $exception->getMessage()
                );
            }
        }

        throw new ContainerException(
            'Зависимость - ' . $key . ': не зарегистрирована в текущем скоупе!'
        );
    }
*/
}